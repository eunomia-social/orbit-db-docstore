# Access Control

Access Control solution for the EUNOMIA project, using the document store module. Add document level granularity to access control.

## Idea

Several auxiliary structures:
- `permissionIndex`: 
- `operationQueue`: 
- `ambiguityQueue`: keeps lists of `create` entries indexed by `objectID`

On `updateIndex`, when going over the log, we will find three different operations. Next is the expected behavior for each of them.


### `create` operation

- add entry to `permissionIndex`
- check if there are objects in the queue for this objectID

- if there is owner ambiguity, ask upper layer to input the owner
  - this means the upper layer queries the blockchain to know who made the initial `create` request
  - "ask upper layer to input the owner" can be done by throwing a specific error and delaying the operation... (clarify)
  - upper layer calls the `verifyOwner` method (for verifying object ownership)
    - `verifyOwner(objectID, ownerID)`: applies the `create` operation for the `objectID` in question made by the entity with ID `ownerID`
    - check that `objectID` has an entry in `ambiguityQueue` and if yes, apply the entry with `entityID == ownerID`
    - after this, we have to go through the log and stop at the first entry we find by the correct owner (to see updated permissions), and then go again to find the latest valid entry by a user with correct permission -- ___`fixObjectState`___
    - keep the knowledge of the current assumed owner -- in case the verified owner is indeed the one assumed at first, we don't need to change anything



`fixObjectState` method:
- invoked if we realize, after ownership ambiguity arises for an object, that the `create` operation used so far was not the original one (i.e., created by the actual owner)
- this relies on the assumption that the timestamp on the blockchain will in fact indicate the order in which operations were made
- go through the log, and might require retrieving parts of the log from ipfs that we do not have yet


### `update` operation
- check if `objectID` already exists in index (i.e., if object already exists)
- if there is an update operation by the owner that changes the permissions of the object, we need to review the current state and verify it is valid
  - if not, we have to revert this
owner changes permissions



### `delete` operation
- do what?

__ATTENTION__: delete operations might have to be reverted if they were done by a user without proper permission (but we only realize this later)
- we have to be able to revert deletes
  - keep all objects in the index even if deleted?
  - keep an index of deleted objects
  - think more about this



## Behavior
Ideas:
- always save previous log and do differences - process only new entries


- start by checking the signature
- then see the type of operation: `create`, `update`, and `delete`

### `create`


### `update`



### `delete`























## Questions, TODOs, and ideas

- change `orbit-http-api` to support additional operations: `create`
- place access controller logic outside DocumentIndex class (to another file)
- and make it possible to use a different AccessController class

- __possible issue:__ object with ID `abc` is successfully deleted, and then another object is created with same ID
  - could be an issue if we keep history of deleted files
  - most importantly, can be a problem if the new `create` arrives at a node before the `delete`

- idea: in updateIndex, go through the log without doing `.reverse`, i.e., from old to new

- Question: when owner ambiguity is detected (and the owner was not yet verified), what should happen to the current state?
  - keep it as is and use first received `create` until `verifyObjectOwner` is invoked
  - keep it as is and use first `create` in the log until `verifyObjectOwner` is invoked
  - delete object from state, and only bring it back once owner is verified

- Add optimizations to avoid re-verifying signatures and permissions every time

- Send all the `create` operations in the snapshot

- Add `*` possibility in permissions to give everyone permissions

- If collaborating with the OrbitDB node, entities can change the object "in the past" - operations can be inserted at any position in the log
- If the OrbitDB node is malicious, it can hold onto operations and insert them in the log whenever it wishes. Either by delaying or inserting them in the "past"

- snapshots can be outdated or say that the last version of the permissions is an entry which is not really the most recent
  - but shouldn't I be able to retrieve the most recent entries of the log?
  - what if I never retrieve it because the snapshot is blocking that replication?
  - how will I ever know that I don't actually have the most recent update by the owner?
  - __We must assume the new node will always receive a "good" snapshot such that it either contains the most recent data or allows the node to correctly rebuild the log in order to eventually reach the correct state__
  - is this assumption too strong?

- start the path search with the heads of the log

- the solution of finding a path from the heads to the create is not necessarily correct...
  - Think of scenarios where there is such a path but the necessary history is not present



# Descrição de possíveis soluções

Access control no módulo do docstore. Assim é feita a verificação de permissões quando está a ser feito o update index. Aqui estão as ideias principais da solução:

Penso que facilitava distinguir a operação de `create` da de `update`. Assim podíamos fazer uma de duas coisas:
1. __-->__ manter uma queue de objetos em espera dos quais não temos a operação de `create`
  - a chegada das operações de `create` dá trigger à verificação e introdução desses objetos no index
2. introduzir no index "cegamente" todos os `updates` que não tenham `create` correspondente
  - quando chega o `create` estes `updates` são verificados

Aqui a 1a opção parece-me a melhor. E para os snapshots manterem a informação da criação, um objeto cujo estado atual seja um `update`, pode ter associado a operação de `create`, e esta é também colocada no snapshot, tal como a ultima operação feita pelo owner para perceber quais as permissoes atuais.

Em relação aos ataques que suscitem ambiguidade do owner:
1. Utilizando os identificadores de objeto dependentes do owner, e.g., `ownerID:objectID`:
  - parece-me que é o mais simples e elegante, e também mais simples de implementar
  - não há ambiguidade em relação a quem criou o objecto
  - sempre que não soubermos quem é o owner, recorremos à blockchain (menor timestamp para aquele `objectID`), mas depois essa informação é cached
  - para evitar alguns delays nos reads, também podemos ir fazendo queries à blockchain (como com as public keys) para ir atualizando essa cache dos tuplos `<objectID, ownerID>`

2. __-->__ Não utilizando IDs dependentes do owner:
  - quando houver uma dúvida sobre quem criou um objeto, precisamos de consultar a blockchain para saber quem é o owner (menor timestamp para aquele `objectID`)
  - depois temos de percorrer o log até chegarmos a uma entrada introduzida pelo owner
  - aí verificamos quais as permissões corretas, e depois percorremos novamente o log até encontrar a entrada mais recente e válida para aquele objecto

Em relação a estas duas alternativas, a 2a demora mais tempo a recuperar de cada vez que surgir uma dessas ambiguidades. Mas assumindo que esse tipo de ataques não será muito comum, esta não exige tantas queries à blockchain.

Estas alterações são feitas no módulo do `orbit-db-docstore`, e depois exigiriam alterações bastante simples na `orbit-db-http-api` e no storage server.



# Some other tests
```js

describe('bad signatures', function () {
  it('entity1 creates object - entity1 and entity2 can write, only entity1 can delete', async function () {
    const writePermissions = [ entityID1, entityID2 ]
    const deletePermissions = [ entityID1 ]

    insertObj = {
      _id: 'obj1',
      data: 'ayayay', // actual object data
      permissions: { // list of permissions
        write: writePermissions, // list of users with write permission
        delete: deletePermissions // list of users with delete permission
      },
      entityID: entityID1, // entity identifier
      // signature, // signature over object with entity's key
    }
    insertObj.signature = await entity1.sign(insertObj)

    const logSize = remoteDatabase._oplog.length
    await localDatabase.put(insertObj)
    await waitForReplication(remoteDatabase._oplog, logSize+1)

    let retrievedObj = (await localDatabase.get('obj1'))[0]
    assert.equal(retrievedObj._id, 'obj1')
    assert.equal(retrievedObj.data, 'ayayay')

    retrievedObj = (await remoteDatabase.get('obj1'))[0]
    assert.equal(retrievedObj._id, 'obj1')
    assert.equal(retrievedObj.data, 'ayayay')
  })

  it('update - change data but keep old signature')
  it('update - entity2 signs but claims to be entity1')
  it('delete - entity2 signs but claims to be entity1')
})

describe('owner can do anything', function () {
  it('entity1 creates object - only entity2 can write and delete', async function () {
    const writePermissions = [ entityID2 ]
    const deletePermissions = [ entityID2 ]

    ...

    insertObj = {
      _id: 'obj2',
      data: 'ayayay', // actual object data
      permissions: { // list of permissions
        write: writePermissions, // list of users with write permission
        delete: deletePermissions // list of users with delete permission
      },
      entityID: entityID1, // entity identifier
      // signature, // signature over object with entity's key
    }
    insertObj.signature = await entity1.sign(insertObj)

    const logSize = remoteDatabase._oplog.length
    await localDatabase.put(insertObj)
    await waitForReplication(remoteDatabase._oplog, logSize+1)

    let retrievedObj = (await localDatabase.get('obj1'))[0]
    assert.equal(retrievedObj._id, 'obj1')
    assert.equal(retrievedObj.data, 'ayayay')

    retrievedObj = (await remoteDatabase.get('obj1'))[0]
    assert.equal(retrievedObj._id, 'obj1')
    assert.equal(retrievedObj.data, 'ayayay')
  })

  it('update - change data but keep old signature')
  it('update - entity2 signs but claims to be entity1')
  it('delete - entity2 signs but claims to be entity1')
})
```

For events:
```js
// ------------ On DocumentIndex.js
const EventEmitter = require('events').EventEmitter

// constructor:
    this.events = new EventEmitter() // TODO add `close` method to remove all listeners


// ------------ On DocumentStore.js
async retrieveAdditionalEntries (entries) {
    await this._replicator.load(entries)
  }
```
