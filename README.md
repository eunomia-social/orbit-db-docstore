Original orbit-db-docstore README [here](./orig-README.md).

# Index snapshot support for OrbitDB - orbit-db-docstore module

## Installation
- Clone and install the `orbit-db-store` repository (steps [here](https://git-ext.inov.pt/cyber/eunomia/orbit-db-store#readme))
- Clone this repository
- Install [node.js and npm](https://docs.npmjs.com/downloading-and-installing-node-js-and-npm) (in case you haven't already)
- Run `npm install`
- Run the following command
  ```
  npm link ../orbit-db-store
  ```


For the `npm link` command to work properly, the directory structure should look like this:
```bash
├── orbit-db
├── orbit-db-docstore
└── orbit-db-store
```
