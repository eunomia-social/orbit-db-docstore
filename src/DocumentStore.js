'use strict'

const Store = require('orbit-db-store')
const DocumentIndex = require('./DocumentIndex')
const pMap = require('p-map')
const Readable = require('readable-stream')

const replaceAll = (str, search, replacement) => str.toString().split(search).join(replacement)

class DocumentStore extends Store {
  constructor (ipfs, id, dbname, options) {
    if (!options) options = {}
    if (!options.indexBy) Object.assign(options, { indexBy: '_id' })
    if (!options.Index) Object.assign(options, { Index: DocumentIndex })
    super(ipfs, id, dbname, options)
    this._type = 'docstore'
  }

  get (key, caseSensitive = false) {
    key = key.toString()
    const terms = key.split(' ')
    key = terms.length > 1 ? replaceAll(key, '.', ' ').toLowerCase() : key.toLowerCase()

    const search = (e) => {
      if (terms.length > 1) {
        return replaceAll(e, '.', ' ').toLowerCase().indexOf(key) !== -1
      }
      return e.toLowerCase().indexOf(key) !== -1
    }
    const mapper = e => this._index.get(e)
    const filter = e => caseSensitive
      ? e.indexOf(key) !== -1
      : search(e)

    return Object.keys(this._index._index)
      .filter(filter)
      .map(mapper)
  }

  getExactKey (key, fullOp = false) {
    const result = this._index.get(key, fullOp)
    return result ? [ result ] : []
  }

  query (mapper, options = {}) {
    // Whether we return the full operation data or just the db value
    const fullOp = options.fullOp || false

    return Object.keys(this._index._index)
      .map((e) => this._index.get(e, fullOp))
      .filter(mapper)
  }

  batchPut (docs, onProgressCallback) {
    const mapper = (doc, idx) => {
      return this._addOperationBatch(
        {
          op: 'PUT',
          key: doc[this.options.indexBy],
          value: doc
        },
        true,
        idx === docs.length - 1,
        onProgressCallback
      )
    }

    return pMap(docs, mapper, { concurrency: 1 })
      .then(() => this.saveSnapshot())
  }

  // TODO: delete this method
  put (doc, options = {}) {
    if (!doc[this.options.indexBy]) { throw new Error(`put: The provided document doesn't contain field '${this.options.indexBy}'`) }

    return this._addOperation({
      op: 'PUT',
      key: doc[this.options.indexBy],
      value: doc
    }, options)
  }

  // TODO: delete this method
  putAll (docs, options = {}) {
    if (!(Array.isArray(docs))) {
      docs = [docs]
    }
    if (!(docs.every(d => d[this.options.indexBy]))) { throw new Error(`The provided document doesn't contain field '${this.options.indexBy}'`) }
    return this._addOperation({
      op: 'PUTALL',
      docs: docs.map((value) => ({
        key: value[this.options.indexBy],
        value
      }))
    }, options)
  }

  // TODO: consider creating the signatures here

  /**
   * Requests the creation of an object
   */
  create (doc, options = {}) {
    if (!doc[this.options.indexBy]) { throw new Error(`create: The provided document doesn't contain field '${this.options.indexBy}'`) }
    if (this._index[doc[this.options.indexBy]]) { throw new Error(`create: The object you are trying to create, already exists`) }

    return this._addOperation({
      op: 'CREATE',
      key: doc[this.options.indexBy],
      value: doc
    }, options)
  }

  /**
   * Requests the update of an object's state
   */
  update (doc, options = {}) {
    if (!doc[this.options.indexBy]) { throw new Error(`update: The provided document doesn't contain field '${this.options.indexBy}'`) }
    // Do we want this? (let it go through and add it to the queue? The queue is meant for updates coming from the network)
    if (this._index[doc[this.options.indexBy]]) { throw new Error(`create: The object you are trying to update, does not exist`) }

    return this._addOperation({
      op: 'UPDATE',
      key: doc[this.options.indexBy],
      value: doc
    }, options)
  }

  /**
   * Requests the deletion of an object's state
   */
  del (key, options = {}) {
    if (!this._index.get(key)) { throw new Error(`No entry with key '${key}' in the database`) }

    return this._addOperation({
      op: 'DEL',
      key: key,
      value: options.value || null
    }, options)
  }

  /**
   * Adds an <id, public key> pair to the public key store
   */
  addKey (id, key) {
    this._index.publicKeys.add(id, key)
  }

  /**
   * Retrieves the public key corresponding to `id` from the public key store
   */
  getKey (id) {
    return this._index.publicKeys.get(id)
  }

  /**
   * Adds the <objID, ownerID> pair as a rule for resolving owner ambiguity. This
   * is relevant for when two `create` operations are received, and the node needs
   * to decide which one is the genuine.
   */
  async verifyObjectOwner (objID, ownerID) {
    this._index.verifyObjectOwner(objID, ownerID)
    await this._index.updateIndex(this._oplog)
  }

  async getIndexSnapshot() {
    const index = Object.values(this._index._index)
    const creates = Object.values(this._index._createIndex)
    const permissions = Object.values(this._index._permissionIndex)

    const snap = {}
    const addToSnap = (e) => {
      if (!snap[e.hash]) snap[e.hash] = e
    }
    index.forEach(addToSnap)
    creates.forEach(addToSnap)
    permissions.forEach(addToSnap)

    return JSON.stringify(snap)
  }
}

module.exports = DocumentStore
