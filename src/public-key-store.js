'use strict'

class PublicKeyStore {
  constructor (options) {
    this.keys = {}
  }

  add (id, key) {
    if (this.keys[id]) {
      throw new Error('PublicKeyStore: You are trying to add a key for an id that already exists')
    }
    this.keys[id] = key
  }

  get (id) {
    return this.keys[id]
  }
}

module.exports = PublicKeyStore
