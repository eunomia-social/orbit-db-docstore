'use strict'
const PublicKeyStore = require('./public-key-store')
const jsonDeterministicStringify = require('json-stringify-deterministic')

const Logger = require('logplease')
const logger = Logger.create('document-store-index', { color: Logger.Colors.Green })
Logger.setLogLevel('ERROR')

function noPermissions (item) {
  return !item.payload.value.permissions ||
         !item.payload.value.permissions.write && !item.payload.value.permissions.delete
}

class DocumentIndex {
  constructor () {
    this._index = {}
    this._createIndex = {}
    this._permissionIndex = {}
    this._verifiedObjectOwners = {}

    this._signatureVerified = {}
    this._nonces = {}

    this.publicKeys = new PublicKeyStore()
  }

  get (key, fullOp = false) {
    return fullOp
      ? this._index[key]
      : this._index[key] ? this._index[key].payload.value : null
  }

  /**
   * Adds the <objID, ownerID> pair as a rule for resolving owner ambiguity. This
   * is relevant for when two `create` operations are received, and the node needs
   * to decide which one is the genuine.
   */
  verifyObjectOwner (objID, ownerID) {
    this._verifiedObjectOwners[objID] = ownerID
  }

  async updateIndex (oplog, onProgressCallback) {

    for (let item of oplog.values.slice()) {

      if (item.payload.op === 'CREATE' && noPermissions(item))
        this._handlePermissionlessCreateOp(item, oplog)

      else if (this._permissionIndex[item.payload.key] === false) {
        if (item.payload.op === 'UPDATE')
          this._handlePermissionlessUpdateOp(item)

        else if (item.payload.op === 'DEL')
          this._handlePermissionlessDeleteOp(item)
      }

      else {
        /* TODO: remove this! Verification might be successful later when the public key arrives */
        if (this._signatureVerified[item.hash] === false) continue

        if (!this._signatureVerified[item.hash] && !(await this._verifySignature(item, oplog._identity.provider))) {
          logger.warn('Signature verification failed. Ignoring entry')
          this._signatureVerified[item.hash] = false
          continue
        }

        if (!this._signatureVerified[item.hash] && !this._verifyFreshness(item)) {
          logger.warn('Freshness verification failed. Ignoring entry')
          this._signatureVerified[item.hash] = false
          continue
        }

        this._signatureVerified[item.hash] = true

        if (item.payload.op === 'CREATE')
          this._handleCreateOp(item, oplog)

        else if (item.payload.op === 'UPDATE')
          this._handleUpdateOp(item)

        else if (item.payload.op === 'DEL')
          this._handleDeleteOp(item)
      }

      if (onProgressCallback) onProgressCallback(item)
    }
  }

  /**
   * Verifies the validity of the request's signature, using the public key present
   * in the class's Public Key Store.
   * @param {LogEntry} entry The entry to be verified
   * @param {IdentityProvider} identityProvider The IdentityProvider object, which
   * contains the method for verifying signatures.
   * @returns {boolean} `true` if the signature is valid, `false` otherwise
   */
  async _verifySignature (entry, identityProvider) {
    // logger.debug(`_verifySignature: verifying signature`)

    const obj = entry.payload.value
    const entityID = obj.entityID
    const publicKey = this.publicKeys.get(entityID)
    if (!publicKey) {
      logger.error(`_verifySignature: no public key present for this entity`)
      return false
    }

    const signature = obj.signature
    if (!signature) {
      logger.error(`_verifySignature: no signature present`)
      return false
    }

    const toSign = jsonDeterministicStringify({
      objectID: entry.payload.key,
      data: obj.data,
      permissions: obj.permissions,
      encrypted: obj.encrypted,
      cleartextProperties: obj.cleartextProperties,
      entityID: entityID,
      nonce: obj.nonce
    })

    return await identityProvider.verify(signature, publicKey, toSign)
  }

  /**
   * Verifies an entry's freshness information.
   * @param {LogEntry} entry The entry to be verified
   * @returns {boolean} `true` if the entry is fresh, `false` otherwise
   */
  _verifyFreshness (entry) {
    const nonce = entry.payload.value.nonce
    if (!nonce) {
      logger.error(`_verifyFreshness: no nonce present`)
      return false
    }

    if (!this._nonces[nonce]) {
      this._nonces[nonce] = true
      return true
    } else {
      logger.error(`_verifyFreshness: nonce seen before`)
      return false
    }
  }

  /**
   * CREATE operation
   */
  _handleCreateOp (item, oplog) {
    // logger.debug(`_handleCreateOp: creating object`)
    const key = item.payload.key

    if (!this._index[key]) {
      this._index[key] = item
      this._createIndex[key] = item
      this._permissionIndex[key] = item

    } else {
      if (this._createIndex[key].hash !== item.hash) {
        logger.debug(`Handling owner ambiguity for object ${key}`)

        if (this._verifiedObjectOwners[key]) {
          // Legitimate owner known

          const entityID = item.payload.value.entityID

          if (this._verifiedObjectOwners[key] === entityID) {
            logger.debug(`Assumed creator was incorrect - fixing state`)

            // Before fixing state, we need to know if we have all the history up until this `create` operation
            if (this.checkPath(oplog, oplog.heads, item)) { // is there a path from the heads to this create op?
              // ATTENTION ATTENTION ATTENTION ATTENTION ATTENTION ATTENTION
              // cannot use heads as starting point... the heads always lead everywhere. Use latest log entry
              // ATTENTION ATTENTION ATTENTION ATTENTION ATTENTION ATTENTION
              logger.error(`Fixing state - all necessary history is available`)
              this._index[key] = item
              this._createIndex[key] = item
              this._permissionIndex[key] = item
            } else {
              logger.error(`Unable to fix state - necessary history is missing. Requesting replication`)
              // get `nexts` in the path that are not in the log
            }
          } else {
            logger.debug(`Illegitimate creator - ignoring entry`)
            // ignore this entry - this creator is not legitimate
          }
        } else {
          // Legitimate owner unknown
          logger.debug(`Legitimate owner unknown - ignoring most recently received create until further action`)
          // TODO: trigger event to let upper layer know that action is necessary
        }
      }
    }
  }

  _isOwner (entry) {
    return this._createIndex[entry.payload.key].payload.value.entityID === entry.payload.value.entityID
  }

  _permissionVerifier (operation) {
    return (entry) => this._isOwner(entry) ||
                  this._permissionIndex[entry.payload.key].payload.value.permissions[operation].includes('*') ||
                  this._permissionIndex[entry.payload.key].payload.value.permissions[operation].includes(entry.payload.value.entityID)
  }

  /**
   * UPDATE operation
   */
  _handleUpdateOp (item) {
    // logger.debug(`_handleUpdateOp: updating object`)
    const hasPermission = this._permissionVerifier('write').bind(this)

    const changedPermissions = (entry) => {
      if (!entry.payload.value.permissions) return false
      return JSON.stringify(this._permissionIndex[entry.payload.key].payload.value.permissions) !==
                                                              JSON.stringify(entry.payload.value.permissions)
    }

    if (this._index[item.payload.key]) {
      if (!hasPermission(item)) {
        logger.warn(`${item.payload.value.entityID} cannot write object ${item.payload.key} (no write permission)`)
        return
      }

      // If it is the owner, allow changing permissions
      if (this._isOwner(item) && changedPermissions(item)) {
        this._permissionIndex[item.payload.key] = item
      }

      this._index[item.payload.key] = item
    } else {
      // ignore update as we have not received the create yet
      logger.warn(`ignoring update on ${item.payload.key} - create was not received yet`)
    }
  }

  /**
   * DELETE operation
   */
  _handleDeleteOp (item) {
    // logger.debug(`_handleDeleteOp: deleting object`)
    const hasPermission = this._permissionVerifier('delete').bind(this)

    if (this._index[item.payload.key]) {
      if (!hasPermission(item)) {
        logger.warn(`${item.payload.value.entityID} cannot delete object ${item.payload.key} (no delete permission)`)
        return
      }

      delete this._index[item.payload.key]
    }
  }

  /**
   * Permissionless CREATE operation
   */
  _handlePermissionlessCreateOp (item, oplog) {
    // logger.debug(`_handlePermissionlessCreateOp: creating object`)
    const key = item.payload.key

    // If object exists, it is only overwritten if it is a permissionless object
    if (!this._index[key] || this._permissionIndex[key] === false) {
      this._index[key] = item
      this._createIndex[key] = item
      this._permissionIndex[key] = false
    }
  }

  /**
   * Permissionless UPDATE operation
   */
  _handlePermissionlessUpdateOp (item) {
    // logger.debug(`_handlePermissionlessUpdateOp: updating object`)
    const key = item.payload.key

    if (this._index[key] && this._permissionIndex[key] === false) {
      this._index[key] = item
    }
  }

  /**
   * Permissionless DELETE operation
   */
  _handlePermissionlessDeleteOp (item) {
    // logger.debug(`_handlePermissionlessDeleteOp: deleting object`)
    const key = item.payload.key

    if (this._index[key] && this._permissionIndex[key] === false) {
      delete this._index[item.payload.key]
    }
  }

  // TODO place this in another file
  //               |
  //               v

  /**
   * Checks if there is a path in the given log connecting any of the `start` entries
   * to the `end` entry. This is done by doing a BFS search in the log by following the
   * `next` references (that are present in the log) successively until either the `end`
   * entry is a `next`, or no `next` entry is present in the log
   *
   * @param {Log} log The log
   * @param {LogEntry[]} start The entries that serve as a starting point for the path
   * @param {LogEntry} end The entry which should be connected to one of the `start`
   * entries by an incoming path.
   *
   * @returns {LogEntry[]|boolean} `true` if there is a path, otherwise returns the dead ends
   */
  checkPath (log, start, end) {
    start = start.map(e => e.hash || e)
    end = end.hash || end

    start = start.filter(e => log.has(e))

    if (start.length === 0) {
      logger.debug('checkPath: None of the given start entries is present in the log')
      return false // TODO throw error
    }

    if (!log.has(end)) {
      logger.debug('checkPath: The given end entry is not present in the log')
      return false // TODO throw error
    }

    if (start.includes(end)) return true

    // // check clocks to make sure `start` actually comes later in the log
    // if (this.entriesDistance(log.get(start), log.get(end)) <= 0) {
    //   // TODO think about this...
    //   // TODO not sure if this is really true
    //   // TODO maybe returning true makes more sense
    //   // TODO make sure you know what the value of the clock means
    //   logger.debug('checkPath: there is no path because distance is non-positive')
    //   return false
    // }

    let deadEnds = []
    let queue = []
    queue = queue.concat(start)

    while (queue.length !== 0) {
      let entryHash = queue.shift()
      let entry = log.get(entryHash)

      for (let e of entry.next) {
        if (e === end) return true

        log.has(e) ? queue.push(e) : deadEnds.push(e)
      }
    }

    // TODO consider returning the entry in the log closest to `end`, so that the next check does not have to start from scratch
    logger.debug('checkPath: No path found')
    return deadEnds
  }

  entriesDistance (a, b) {
    let distance = a.clock.time - b.clock.time
    // if (distance === 0 && a.clock.id !== b.clock.id) return a.clock.id < b.clock.id ? -1 : 1
    return distance
  }
}

module.exports = DocumentIndex
